﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        byte[] GbytesSign;

        public Form1()
        {
            InitializeComponent();
            String[] args = new String[2];

            args[0] = "4608503";
            args[1] = "06565029";

            //String okey = "<RSAKeyValue><Modulus>sQ6fuC2FSTdMXy1VZ2ppgFUAoDPzjSlAo6VqEPL+g7ZJQZth12ASHhVSPDnvkIawKY3NyCuv8p16/86oeaNb8tF5liLvYFQzSMY7dnHI+qz5t8kc1AFR1muSy7yU/ueMuM40v8UpFaXltfcQh+iEoD4Bcb0za3WkeLpqSFcgBjbbQyi4klGaYwa6jco4HhcZP2MhVi4BTL2BRjrTCM0xEGas1g7jyW37vyR3P5Q+Xw6lY+jvVktd4hOm8x29UX+NUazJYv9RK9RS8NSMKcl3Lvh+kV+epOtBSCFMKf9sNZh+J+ce2cVzrfxB2ZJ8sWoIn+Osl2CpDWj0zKvB9lH5Jw==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            //textBox1.Text = SignData(okey, args);

            //var verified = VerifyData(key,args);

            //MessageBox.Show(verified.ToString());
        }

        private bool VerifyData(string key, IEnumerable<string> args)
        {
            var sb = new StringBuilder();
            foreach (var arg in args)
            {
                if (string.IsNullOrEmpty(arg)) continue;
                sb.Append(arg).Append("&");
            }
            sb.Remove(sb.Length - 1, 1);
            var provider = new RSACryptoServiceProvider();

            provider.FromXmlString(key);
            var bytes = Encoding.UTF8.GetBytes(sb.ToString());
            var hashProvider = HashAlgorithm.Create("SHA1");
            return provider.VerifyData(bytes, hashProvider, GbytesSign);

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {


        }

        public string SignData(string key, IEnumerable<string> args)
         {
                var HashProvider = HashAlgorithm.Create("SHA1");
                var sb = new StringBuilder();
                foreach (var arg in args)
                {
                    if (string.IsNullOrEmpty(arg)) continue;
                    sb.Append(arg).Append("&");
                }
                sb.Remove(sb.Length - 1, 1);

                var provider = new RSACryptoServiceProvider();

                provider.FromXmlString(key);

                var bytes = Encoding.UTF8.GetBytes(sb.ToString());
                var bytesSign = provider.SignData(bytes, HashProvider);

                GbytesSign = bytesSign;

                return BytesToString(bytesSign);
         }

        protected string BytesToString(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String privateKey = textBox2.Text;

            privateKey = privateKey.Replace("\n", "").Replace("\r", "").Replace(" ", "");

            String[] args = new String[2];

            args[0] = textBox4.Text;
            args[1] = textBox5.Text;

            textBox1.Text = SignData(textBox2.Text, args);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            String publicKey = textBox3.Text;
            
            String[] args = new String[2];

            args[0] = textBox4.Text;
            args[1] = textBox5.Text;
            publicKey = publicKey.Replace("\n", "").Replace("\r", "").Replace(" ", "");
            bool verified = VerifyData(publicKey, args);
            if (verified == true)
            {
                MessageBox.Show("Всё ОК");
            }
            else
            {
                MessageBox.Show("Всё плохо");
            }
        }
        
    }


    // Helper class for using pem key format
    internal static class PEMToX509
    {
        const string KEY_HEADER = "-----BEGIN RSA PRIVATE KEY-----";
        const string KEY_FOOTER = "-----END RSA PRIVATE KEY-----";

        internal static RSACryptoServiceProvider GetRSA(string pem)
        {
            RSACryptoServiceProvider rsa = null;

            if (IsPrivateKeyAvailable(pem))
            {

                string keyFormatted = pem;

                int cutIndex = keyFormatted.IndexOf(KEY_HEADER);
                keyFormatted = keyFormatted.Substring(cutIndex, keyFormatted.Length - cutIndex);
                cutIndex = keyFormatted.IndexOf(KEY_FOOTER);
                keyFormatted = keyFormatted.Substring(0, cutIndex + KEY_FOOTER.Length);
                keyFormatted = keyFormatted.Replace(KEY_HEADER, "");
                keyFormatted = keyFormatted.Replace(KEY_FOOTER, "");
                keyFormatted = keyFormatted.Replace("\r", "");
                keyFormatted = keyFormatted.Replace("\n", "");
                keyFormatted = keyFormatted.Trim();

                byte[] privateKeyInDER = System.Convert.FromBase64String(keyFormatted);

                rsa = DecodeRSAPrivateKey(privateKeyInDER);
            }

            return rsa;
        }

        private static bool IsPrivateKeyAvailable(string privateKeyInPEM)
        {
            return (privateKeyInPEM != null && privateKeyInPEM.Contains(KEY_HEADER)
                && privateKeyInPEM.Contains(KEY_FOOTER));
        }

        //------- Parses binary ans.1 RSA private key; returns RSACryptoServiceProvider  ---
        public static RSACryptoServiceProvider DecodeRSAPrivateKey(byte[] privkey)
        {
            byte[] MODULUS, E, D, P, Q, DP, DQ, IQ;

            // ---------  Set up stream to decode the asn.1 encoded RSA private key  ------
            MemoryStream mem = new MemoryStream(privkey);
            BinaryReader binr = new BinaryReader(mem);    //wrap Memory Stream with BinaryReader for easy reading
            byte bt = 0;
            ushort twobytes = 0;
            int elems = 0;
            try
            {
                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();        //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();       //advance 2 bytes
                else
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes != 0x0102) //version number
                    return null;
                bt = binr.ReadByte();
                if (bt != 0x00)
                    return null;


                //------  all private key components are Integer sequences ----
                elems = GetIntegerSize(binr);
                MODULUS = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                E = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                D = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                P = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                Q = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                DP = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                DQ = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                IQ = binr.ReadBytes(elems);

                // ------- create RSACryptoServiceProvider instance and initialize with private key -----
                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                RSAParameters RSAparams = new RSAParameters();
                RSAparams.Modulus = MODULUS;
                RSAparams.Exponent = E;
                RSAparams.D = D;
                RSAparams.P = P;
                RSAparams.Q = Q;
                RSAparams.DP = DP;
                RSAparams.DQ = DQ;
                RSAparams.InverseQ = IQ;
                RSA.ImportParameters(RSAparams);
                return RSA;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                binr.Close();
            }
        }

        private static int GetIntegerSize(BinaryReader binary)
        {
            byte bt = 0;
            byte lowbyte = 0x00;
            byte highbyte = 0x00;
            int count = 0;

            bt = binary.ReadByte();

            if (bt != 0x02)
                return 0;

            bt = binary.ReadByte();

            if (bt == 0x81)
                count = binary.ReadByte();
            else if (bt == 0x82)
            {
                highbyte = binary.ReadByte();
                lowbyte = binary.ReadByte();
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };
                count = BitConverter.ToInt32(modint, 0);
            }
            else
                count = bt;

            while (binary.ReadByte() == 0x00)
                count -= 1;

            binary.BaseStream.Seek(-1, SeekOrigin.Current);

            return count;
        }
    }

}
